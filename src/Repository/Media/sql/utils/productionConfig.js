import { readFileSync } from "fs";

export default function productionConfig(){

    return {
        ssl:{
            ca:readFileSync("./DigiCertGlobalRootCA.crt.pem")
        },
        password:process.env.LOGIC_SQL_PASSWORD, 
    }

}