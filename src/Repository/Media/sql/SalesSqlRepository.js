import SqlRepository from "../../../Utils/Repository/Sql/repository/SqlRepository.js";
import mysql2 from "mysql2";
import productionConfig from "./utils/productionConfig.js";

export default class SalesSqlRepository extends SqlRepository{

    static repository = mysql2.createPool({
        host:process.env.LOGIC_SQL_HOST,
        user:process.env.LOGIC_SQL_USER,
        database:process.env.LOGIC_SQL_DB,
        waitForConnections:true,
        connectionLimit:10,
        maxIdle:10,
        idleTimeout:6000,
        queueLimit:0,
        enableKeepAlive:true,
        keepAliveInitialDelay:0,
        ...(process.env.PRODUCTION?productionConfig():{})
    });

    constructor(){

        super();

    }

    execute(response,data){

        this.executeProccess(response,data,SalesSqlRepository.repository);

    }

}