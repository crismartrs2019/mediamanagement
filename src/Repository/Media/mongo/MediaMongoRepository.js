import MongoDbRepository from "../../../Utils/Repository/Mongo/repository/MongoRepository.js";
import MongoDbObject from "../../../Utils/Repository/Mongo/repository/MongoDbObject.js";

export default class MediaMongoRepository extends MongoDbRepository{

    static repository = new MongoDbObject({
        uri:"mongodb://127.0.0.1:27017/",
        options:{
            socketTimeoutMs:500
        },
        database:"media_management"
    });

    constructor(){

        super();

    }

    getRepository(){

        return MediaMongoRepository.repository;

    }


}