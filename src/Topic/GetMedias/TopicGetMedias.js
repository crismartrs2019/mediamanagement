import Operation from "../../Utils/Operation/main/Operation.js";

export default class TopicGetMedias extends Operation{

    #media = null;

    #page = null;

    constructor(FilterGetMedias,Page){

        super();

        if(FilterGetMedias.isValid()==false){

            this.abortOperation();

            return;

        }

        this.#media = FilterGetMedias.getData();

        this.#page = Page;

    }


    async main(response){

        try{

            const result = await this.repository().getMedias(this.#media,this.#page);

            if(!result[0]){

                response.error({error:404,message:"Topic not found"});

                return;
            }

            response.ok({medias:result[0]["medias"]});

        }catch(err){

            console.log(err);

            response.error({error:505,message:"Something went wrong"});

        }

    }


}