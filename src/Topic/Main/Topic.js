import UserIsType from "../../User/Validations/userIsType.js";
import Module from "../../Utils/Modules/Module.js";
import MediaFilterAdd from "../AddMedia/Filter/MediaFilterAdd.js";
import TopicAddMedia from "../AddMedia/TopicAddMedia.js";
import TopicFilterCreate from "../Create/Filter/TopicFilterCreate.js";
import TopicCreate from "../Create/TopicCreate.js";
import TopicFilterDelete from "../Delete/Filter/TopicFilterDelete.js";
import TopicDelete from "../Delete/TopicDelete.js";
import FilterGetMedias from "../GetMedias/Filter/FilterGetMedias.js";
import TopicGetMedias from "../GetMedias/TopicGetMedias.js";
import TopicFilterList from "../List/Filter/TopicFilterList.js";
import TopicList from "../List/TopicList.js";
import TopicFilterRead from "../Read/Filter/TopicFilterRead.js";
import TopicRead from "../Read/TopicRead.js";
import TopicRepository from "../Repository/TopicRepository.js";

export default class Topic extends Module{


    constructor(){

        super();

    }

    create(TopicData,User){

        const filter = new TopicFilterCreate(TopicData);

        const operation = new TopicCreate(filter);

        operation.setRepository(TopicRepository);

        operation.setValidations([UserIsType(User,["admin","creator"],this.module("user"))]);

        return operation.do();

    }

    read(Entity){

        const filter = new TopicFilterRead(Entity);

        const operation = new TopicRead(filter);

        operation.setRepository(TopicRepository);

        return operation.do()

    }

    addMedia(Topic,Media,User){

        const filter = new MediaFilterAdd(Topic,Media);

        const operation = new TopicAddMedia(filter);

        operation.setRepository(TopicRepository);

        operation.setValidations([UserIsType(User,["admin","creator"],this.module("user"))]);

        return operation.do();

    }

    delete(Entity,User){

        const filter = new TopicFilterDelete(Entity);

        const operation = new TopicDelete(filter);

        operation.setRepository(TopicRepository);

        operation.setValidations([UserIsType(User,["admin"],this.module("user"))]);

        return operation.do();

    }

    list(Page){

        const operation = new TopicList(Page);

        operation.setRepository(TopicRepository);

        return operation.do();

    }

    getMedias(Entity,Page){

        const filter = new FilterGetMedias(Entity);

        const operation = new TopicGetMedias(filter,Page);

        operation.setRepository(TopicRepository);

        return operation.do();

    }

}