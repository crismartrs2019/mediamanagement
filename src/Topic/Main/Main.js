import TopicApi from "./Entry/TopicApi.js";
import TopicModule from "./Module/TopicModule.js";

export default function TopicMain(ModuleManager,ApiExpress){

    TopicModule(ModuleManager);

    ApiExpress.use("/topic",TopicApi(ModuleManager.module("topic")));

}