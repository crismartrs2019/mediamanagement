import { Router } from "express";
import TopicData from "../../Data/TopicData.js";
import Entity from "../../../Utils/Entity/Entity.js";
import ExpressResponse from "../../../Utils/Response/express/ExpressResponse.js";
import Page from "../../../Utils/Data/Page.js";

export default function TopicApi(TopicModule){

    const router = new Router();

    router.post("/",(req,res)=>{

        const topic = new TopicData(req.body["topic"]);

        const user = new Entity(req.body["user"]);

        TopicModule.create(topic,user).then((result)=>{

            (new ExpressResponse(res)).result(result);

        }).catch((err)=>{

            (new ExpressResponse(res)).error(err);

        }); 

    });

    router.get("/list",(req,res)=>{

        const page = new Page(req.query["page"]);

        TopicModule.list(page).then((result)=>{

            (new ExpressResponse(res)).result(result);

        }).catch((err)=>{

            (new ExpressResponse(res)).error(err);

        });

    });

    router.get("/:idTopic",(req,res)=>{

        const topic = new Entity(req.params["idTopic"]);

        TopicModule.read(topic).then((result)=>{

            (new ExpressResponse(res)).result(result);

        }).catch((err)=>{

            (new ExpressResponse(res)).error(err);

        })

    });


    router.post("/:idTopic/media/:idMedia/add",(req,res)=>{

        const topic = new Entity(req.params["idTopic"]);

        const media = new Entity(req.params["idMedia"]);

        const user = new Entity(req.body["user"]);

        TopicModule.addMedia(topic,media,user).then((result)=>{

            (new ExpressResponse(res)).result(result);

        }).catch((err)=>{

            (new ExpressResponse(res)).error(err);

        })

    })

    router.post("/:idTopic/media/list",(req,res)=>{

        const topic = new Entity(req.params["idTopic"]);

        const page = new Page(req.query["page"]);

        TopicModule.getMedias(topic,page).then((result)=>{

            (new ExpressResponse(res)).result(result);

        }).catch((err)=>{

            (new ExpressResponse(res)).error(err);

        });

    });


    return router;

}