import TopicMongo from "./Mongo/TopicMongo.js";

export default class TopicRepository{

    static mongoRepository = new TopicMongo();

    static create(TopicData){

        return TopicRepository.mongoRepository.create(TopicData);

    }

    static search(TopicData,Page){

        return TopicRepository.mongoRepository.search(TopicData,Page);

    }

    static read(TopicData){

        return TopicRepository.mongoRepository.getData(TopicData);

    }

    static delete(Entity){

        return TopicRepository.mongoRepository.delete(Entity);

    }

    static addMedia(Topic,Media){

        return TopicRepository.mongoRepository.addMedia(Topic,Media);

    }

    static list(Page){

        return TopicRepository.mongoRepository.list(Page);

    }

    static search(Topic,Page){

    }

    static getMedias(Topic,Page){
    
        return TopicRepository.mongoRepository.getMedias(Topic,Page)

    }

}