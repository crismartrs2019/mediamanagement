import MediaMongoRepository from "../../../Repository/Media/mongo/MediaMongoRepository.js";
import MongoController from "../../../Utils/Repository/Mongo/main/MongoController.js";

export default class TopicMongo extends MongoController{

    repository = new MediaMongoRepository();

    constructor(){

        super();

    }


    queryCreate(Topic){

        return {
            query:[
                {
                    _id:Topic.getPureId(),
                    name:Topic.getName(),
                    types:Topic.getTypes(),
                    medias:[]
                }
            ],
            collection:"topic"
        }

    }


    queryDelete(Entity){

        return {
            query:{_id:Entity.getPureId()},
            collection:"topic"
        }

    }   

    addMedia(Topic,Media){
    
        const query = {

            query:{_id:Topic.getPureId()},
            update:{$push:{medias:Media.getId()}},
            collection:"topic"
        }

        return this.update(query);

    }

    list(Page){

        const query = {
            query:{},
            options:{skip:Page.page(),limit:10},
            collection:"topic"
        }

        return this.read(query);

    }


    getMedias(Topic,Page){

        const query = {

            query:{
                _id:Topic.getPureId()
            },
            options:{
                projection:{"medias":{$slice:[Page.page(),10]}}
            },
            collection:"topic"
        }

        return this.read(query);

    }

    search(Topic,Page){

        const query = {
            query:{
                name:Topic.getName()
            },
            options:{skip:Page.page(),limit:10},
            collection:"topic"
        }

        return this.read(query);

    }

    getData(Topic){

        const query = {
            query:{_id:Topic.getPureId()},
            collection:"topic"
        }

        return this.read(query);

    }

    queryUpdate(Query){

        return Query;

    }

    queryRead(Query){

        return Query;

    }

}