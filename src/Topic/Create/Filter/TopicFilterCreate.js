import BaseEntity from "../../../Utils/Entity/BaseEntity.js";
import basicFilter from "../../../Utils/Filters/BasicFilter.js";

export default class TopicFilterCreate extends BaseEntity{

    constructor(Topic){

        super(Topic);
        
    }

    #evalData(){

        const topic = this.getData();

        if(basicFilter(topic.getName()).required().isType("string").isValid()==false){

            this.invalidate();

            return;

        }

        if(basicFilter(topic.getTypes()).required().isType("object").value((types)=>types.length>0).isValid()==false){

            this.invalidate();

            return;

        }

        if(basicFilter(topic.getPureId()).required().isValid()==false){


            this.invalidate();

            return;

        }

    }

}