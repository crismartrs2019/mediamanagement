import Operation from "../../Utils/Operation/main/Operation.js";
import TopicData from "../Data/TopicData.js";
import Page from "../../Utils/Data/Page.js";

export default class TopicCreate extends Operation{

    #topic = false;

    constructor(TopicFilterCreate){

        super();

        if(TopicFilterCreate.isValid()==false){

            this.abortOperation();

            return;

        }

        this.#topic = TopicFilterCreate.getData();

    }

    async main(response){

        try{

            const topicToSearch = new TopicData({name:this.#topic.getName(),id:"noId"});

            const topicExists = await this.repository().search(topicToSearch, new Page(1));

            if(topicExists.length>0){

                response.error({code:505,message:"Topic already exists"});

                return;
            }

            const result = await this.repository().create(this.#topic);

            response.ok({id:this.#topic.getId()});

        }catch(err){

            response.error(err);

        }

    }

}
