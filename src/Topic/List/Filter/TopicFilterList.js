import BaseEntity from "../../../Utils/Entity/BaseEntity.js";
import basicFilter from "../../../Utils/Filters/BasicFilter.js";

export default class TopicFilterList extends BaseEntity{

    constructor(Topic){

        super(Topic);

        this.#eval();

    }

    #eval(){

        const topic = this.getData();

        if(basicFilter(topic.getPureId()).required().isValid()==false){

            this.invalidate();

            return;
        }


        if(basicFilter(topic.getName()).isType("string").isValid()==false){

            this.invalidate();

            return;

        }

    }

}