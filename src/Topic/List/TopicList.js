import Operation from "../../Utils/Operation/main/Operation.js";

export default class TopicList extends Operation{

    #page = null;


    constructor(Page){

        super();

        this.#page = Page;    


    }

   async main(response){

        try{

            const result = await this.repository().list(this.#page);

            response.ok({result:result});

        }catch(err){

            console.log(err);

            response.error({error:505,message:"something went wrong"});

        }

    }

}