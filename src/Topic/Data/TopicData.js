import Entity from "../../Utils/Entity/Entity.js";

export default class TopicData extends Entity{

    #name = null;

    #fileTypes = [];

    constructor(data){

        super(data["id"]);

        this.#name = data["name"];

        this.#fileTypes = data["fileTypes"];

    }

    getName(){

        return this.#name;

    }

    getTypes(){

        return this.#fileTypes;


    }

}