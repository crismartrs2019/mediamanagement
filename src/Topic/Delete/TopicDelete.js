import Operation from "../../Utils/Operation/main/Operation.js";

export default class TopicDelete extends Operation{

    #topic = null;

    constructor(TopicFilterDelete){

        super();

        if(TopicFilterDelete.isValid()==false){

            this.abortOperation();

            return;

        }

        this.#topic = TopicFilterDelete.getData();

    }

    async main(response){

        try{

            await this.repository().delete(this.#topic);

            response.ok({id:this.#topic.getId()});

        }catch(err){

            console.log(err);

            response.error({error:505,message:"Something went wrong"});

        }

    }

}