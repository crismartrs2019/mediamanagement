import BaseEntity from "../../../Utils/Entity/BaseEntity.js";
import basicFilter from "../../../Utils/Filters/BasicFilter.js";

export default class TopicFilterDelete extends BaseEntity{
 
    constructor(data){

        super(data);

        this.#eval();

    }

    #eval(){

        const topic = this.getData();

        if(basicFilter(topic.getPureId()).required().isValid()==false){

            this.invalidate();

            return;

        }

    
    }

}