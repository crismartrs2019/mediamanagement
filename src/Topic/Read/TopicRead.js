import Operation from "../../Utils/Operation/main/Operation.js";

export default class TopicRead extends Operation{

    #topic = null;

    constructor(TopicFilterRead){

        super();

        if(TopicFilterRead.isValid()==false){
            
            this.abortOperation();
            
            return;
        
        }

        this.#topic = TopicFilterRead.getData();

    }

    async main(response){

        try{
            
            const result = await this.repository().read(this.#topic);

            if(!result[0]){

                response.error({error:404,message:"Media not found"});

                return;
            }

            response.ok({id:result[0]._id.toString(),name:result[0].name,types:result[0]["types"]});

        }catch(err){

            console.log(err);

            response.error({error:505,message:"Something went wrong"});

        }

    }

}