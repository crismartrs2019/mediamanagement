import Operation from "../../Utils/Operation/main/Operation.js";

export default class TopicAddMedia extends Operation{

    #add = null;

    constructor(MediaFilterAdd){

        super();

        if(MediaFilterAdd.isValid()==false){

            this.abortOperation();

            return;
        }

        this.#add = MediaFilterAdd.getData();

    }


    async main(response){

        try{

            const result = await this.repository().addMedia(this.#add["topic"],this.#add["media"]);

            response.ok({addedTo:this.#add["topic"].getId(),media:this.#add["media"].getId()});

        }catch(err){

            console.log(err);

            response.error({error:505,message:"Something went wrong"});

        }

    }

}