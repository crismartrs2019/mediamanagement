import BaseEntity from "../../../Utils/Entity/BaseEntity.js";
import Entity from "../../../Utils/Entity/Entity.js";
import basicFilter from "../../../Utils/Filters/BasicFilter.js";

export default class MediaFilterAdd extends BaseEntity{

    constructor(Topic,Media){

        super({topic:Topic,media:Media});

        this.#eval();

    }

    #eval(){

        const data = this.getData();

        if(basicFilter(data["topic"]).isType("object").required().value((topic)=>topic instanceof Entity).isValid()==false){

            this.invalidate();

            return;
        }

        if(basicFilter(data["topic"].getPureId()).required().isValid()==false){

            this.invalidate();

            return;

        }

        if(basicFilter(data["media"]).isType("object").required().value((topic)=>topic instanceof Entity).isValid()==false){

            this.invalidate();

            return;
        }

        if(basicFilter(data["media"].getPureId()).required().isValid()==false){

            this.invalidate();

            return;

        }

    }

}