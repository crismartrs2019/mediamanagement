import MediaLocal from "./Local/MediaLocal.js";
import MediaMongo from "./Mongo/MediaMongo.js";

export default class MediaRepository{

    static mongoRepository = new MediaMongo();

    static fileRepository = new MediaLocal();

    static create(Media){

        return Promise.all([
            MediaRepository.fileRepository.create(Media.getFile(),Media.getId()+"."+Media.getType()),
            MediaRepository.mongoRepository.create(Media)
        ]);

    }

    static list(Page){

        return MediaRepository.mongoRepository.list(Page);

   }    

   static read(Media){

        return MediaRepository.mongoRepository.getData(Media);

   }

}