import MediaMongoRepository from "../../../Repository/Media/mongo/MediaMongoRepository.js";
import MongoController from "../../../Utils/Repository/Mongo/main/MongoController.js";

export default class MediaMongo extends MongoController{

    repository = new MediaMongoRepository();

    queryCreate(Media){

        return {
            query:[{_id:Media.getPureId(),name:Media.getName(),type:Media.getType()}],
            collection:"media"
        }

    }   


    list(Page){

        const query = {

            query:{},
            options:{skip:Page.page(),limit:10},
            collection:"media"

        }

        return this.read(query);

    }

    getData(Media){

        const query = {
            query:{_id:Media.getPureId()},
            collection:"media"
        }

        return this.read(query);

    }

    queryDelete(){

    }

    queryRead(query){

        return query;

    }

}