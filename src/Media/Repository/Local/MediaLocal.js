import LocalRepository from "../../../Utils/Repository/Local/LocalRepository.js";


export default class MediaLocal extends LocalRepository{

    static basePath = process.env.FILE_BASE_PATH;

    constructor(){
    
        super();

    }

    queryCreate(){

        return MediaLocal.basePath;

    }

}