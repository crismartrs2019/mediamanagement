import MediaCreateFilter from "../../Create/Filter/MediaCreateFilter.js";
import MediaData from "../../Data/MediaData.js";
import MediaCreateFakeRepository from "./Repository/MediaCreateFake.js";
import assert from "../../../Utils/Test/Assert.js";
import MediaCreate from "../../Create/MediaCreate.js";

export default class MediaCreateCase{

    

    static async filterInvalid(){

        try{    

            const media = new MediaData({});

            const filter = new MediaCreateFilter(media);

            const create = new MediaCreate(filter);

            const repository = new MediaCreateFakeRepository();

            create.setRepository(repository);

            repository.setCallTo("create","mediaOk");

            const result = await create.do();

            assert(result["id"]!=undefined,"Media filter invalid");

            console.log(result);

        }catch(err){

            assert(false,"Media create case ok");
            console.log(err);
        }

        

    }

    static  async ok(){

        try{    

            const media = new MediaData({name:"some media"});

            const filter = new MediaCreateFilter(media);

            const create = new MediaCreate(filter);

            const repository = new MediaCreateFakeRepository();

            create.setRepository(repository);

            repository.setCallTo("create","mediaOk");

            const result = await create.do();

            assert(result["id"]!=undefined,"Media create case ok");

            console.log(result);

        }catch(err){

            assert(false,"Media create case ok");
            console.log(err);
        }

    }

    static main(){

        MediaCreateCase.ok();

    }

}