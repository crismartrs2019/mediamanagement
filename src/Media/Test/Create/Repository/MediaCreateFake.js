import FakeRepository from "../../../../Utils/Repository/Fake/FakeRepository.js";

export default class MediaCreateFakeRepository extends FakeRepository{

    constructor(){
        super();
    }


    mediaOk(Media){

        return new Promise((resolve,reject)=>{

            resolve({id:Media.getId()});

        });
    
    }


    create(Media){

        return this.callTo("create")(Media);

    }

}