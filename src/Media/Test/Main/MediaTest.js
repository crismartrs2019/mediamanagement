import MediaCreateCase from "../Create/MediaCreateCase.js";
import MediaCreate from "../../Create/MediaCreate.js";
import MediaCreateFilter from "../../Create/Filter/MediaCreateFilter.js";
import MediaData from "../../Data/MediaData.js";
import assert from "../../../Utils/Test/Assert.js";

export default class MediaTest{



    static async create(){

        MediaCreateCase.main();        

    }

    static delete(){

    }

    static read(){

    }

    static dateGenerateId(){

        const media = new MediaData({name:"some media file"});

        assert(media.getPureId(),"Media data generates id");
        console.log("Id:"+media.getId());
        console.log("Name:"+media.getName());

    }

    static dataRejectId(){
        
        const media = new MediaData({id:"220394",name:"some media file"});

        assert(media.getPureId()==null,"Media data reject id");
        console.log("Id:"+media.getPureId());
        console.log("Name:"+media.getName());

    }

    static main(){

        MediaTest.dateGenerateId();

        MediaTest.dataRejectId();

        MediaTest.create();

    }


}