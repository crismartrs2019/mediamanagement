import Entity from "../../Utils/Entity/Entity.js";

export default class MediaData extends Entity{

    #data = false;

    constructor(data){

        super(data["id"]);

        this.#data = {

            name: data["name"],
            media: data["media"],
            file: data["file"],
            type: data["type"]
        }           

    }

    getName(){

        return this.#data["name"];

    }

    getMedia(){

        return this.#data["media"];

    }

    getType(){

        return this.#data["type"];

    }

    getFile(){

        return this.#data["file"];

    }

}