import BaseEntity from "../../../Utils/Entity/BaseEntity.js";
import basicFilter from "../../../Utils/Filters/BasicFilter.js";

export default class MediaFilterList extends BaseEntity{


    constructor(MediaData){

        super(MediaData);

    }


    #eval(){

        const media = this.getData();

        if(basicFilter(media.getName()).isType("string").isValid()==false){
            
            this.invalidate();

            return;
        }

    }   


}