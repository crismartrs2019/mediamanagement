import MediaEntry from "./Entry/MediaEntry.js";
import MediaModule from "./Modules/ModuleConfig.js";

export default function MediaMain(ModuleManager,ApiExpress){


    MediaModule(ModuleManager);

    ApiExpress.use("/media",MediaEntry(ModuleManager.module("media")));

}