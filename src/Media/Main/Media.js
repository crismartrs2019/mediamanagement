import UserIsType from "../../User/Validations/userIsType.js";
import Module from "../../Utils/Modules/Module.js";
import MediaCreateFilter from "../Create/Filter/MediaCreateFilter.js";
import MediaCreate from "../Create/MediaCreate.js";
import MediaRepository from "../Repository/MediaRepository.js";
import MediaList from "../List/MediaList.js";
import MediaFilterList from "../List/Filter/MediaFilterList.js";
import MediaFilterRead from "../Read/Filter/MediaFilterRead.js";
import MediaRead from "../Read/MediaRead.js";

export default class Media extends Module{

    constructor(){

        super();

    }


    create(Media,UserData){

    
        const mediaFilter = new MediaCreateFilter(Media);

        const createOperation = new MediaCreate(mediaFilter);

        createOperation.setRepository(MediaRepository);

        createOperation.setValidations([UserIsType(UserData,["admin","creator"],this.module("user"))]);

        return createOperation.do();

    }

    read(Entity){

        const filter = new MediaFilterRead(Entity);

        const operation = new MediaRead(filter);

        operation.setRepository(MediaRepository);

        return operation.do();

    }

    list(Page){

        const operation = new MediaList(Page);
        
        operation.setRepository(MediaRepository);

        return operation.do();

    }

    delete(){

    }


}