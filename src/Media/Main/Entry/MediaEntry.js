import {Router} from "express";
import MediaData from "../../Data/MediaData.js";
import Entity from "../../../Utils/Entity/Entity.js";
import ExpressResponse from "../../../Utils/Response/express/ExpressResponse.js";
import Page from "../../../Utils/Data/Page.js";

export default function MediaEntry(MediaModule){

    const router = new Router();

        
    router.post("/",(req,res)=>{

        const media = new MediaData(req.body["media"]);

        const user = new Entity(req.body["user"]);

        MediaModule.create(media,user).then((result)=>{

            (new ExpressResponse(res)).result(result);


        }).catch((err)=>{

            (new ExpressResponse(res)).error(err);

        });


    });



    router.get("/list",(req,res)=>{


        const page = new Page(req.query["page"]);

        MediaModule.list(page).then((result)=>{

            (new ExpressResponse(res)).result(result);

        }).catch((err)=>{

            (new ExpressResponse(res)).error(err);

        });

    });

    router.get("/:id",(req,res)=>{

        const media = new Entity(req.params["id"]);

        MediaModule.read(media).then((result)=>{

            (new ExpressResponse(res)).result(result);

        }).catch((err)=>{

            (new ExpressResponse(res)).error(err);

        });

    });


    return router;
}