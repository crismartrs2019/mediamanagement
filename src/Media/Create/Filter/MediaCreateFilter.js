import BaseEntity from "../../../Utils/Entity/BaseEntity.js";
import basicFilter from "../../../Utils/Filters/BasicFilter.js";

export default class MediaCreateFilter extends BaseEntity{

    constructor(Media){

        super(Media);

        this.#evalMedia();

    }


    #evalMedia(){

        const media = this.getData();


        if(basicFilter(media.getName()).isType("string").required().value((val)=>val!="").isValid()==false){
            
            this.invalidate("Name is not correct");
            
            return;
        }

        if(basicFilter(media.getId()).required().isValid()==false){

            this.invalidate("id is not ok");

            return;
        }

    }
}