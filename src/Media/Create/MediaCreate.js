import Operation from "../../Utils/Operation/main/Operation.js";

export default class MediaCreate extends Operation{

    #media = false;

    constructor(MediaCreateFilter){

        super();

        if(MediaCreateFilter.isValid()==false){

            this.abortOperation();

            return;
        }   

        this.#media = MediaCreateFilter.getData();

    }


    async main(response){

        try{

            await this.repository().create(this.#media);

            response.ok({id:this.#media.getId()});


        }catch(err){

            console.log(err);

            response.error({code:505,message:"something went wrong"});

        }

    }

}