import Operation from "../../Utils/Operation/main/Operation.js";

export default class MediaRead extends Operation{

    #media = null;

    constructor(MediaFilterRead){

        super();

        if(MediaFilterRead.isValid()==false){
            
            this.abortOperation();
            
            return;
        
        }

        this.#media = MediaFilterRead.getData();

    }

    async main(response){

        try{
            
            const result = await this.repository().read(this.#media);

            if(result.length==0){

                response.error({error:404,message:"Media not found"});

                return;
            }

            response.ok({id:result[0]._id.toString(),name:result[0].name,type:result[0].type});

        }catch(err){

            console.log(err);

            response.error({error:505,message:"Something went wrong"});

        }

    }

}