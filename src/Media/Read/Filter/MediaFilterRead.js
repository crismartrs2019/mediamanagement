import BaseEntity from "../../../Utils/Entity/BaseEntity.js";
import basicFilter from "../../../Utils/Filters/BasicFilter.js";

export default class MediaFilterRead extends BaseEntity{

    constructor(MediaData){

        super(MediaData);

        this.#eval();

    }

    #eval(){

        const media = this.getData();

        if(basicFilter(media.getPureId()).required().isValid()==false){

            this.invalidate();

            return;

        }

    }


}