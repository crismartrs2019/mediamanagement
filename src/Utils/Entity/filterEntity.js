export default function validEntity(entity,type){

    if((entity instanceof type)==false){
        console.log("not instance of");
        return false;
    }


    if(entity.isValid()==false){

        console.log("Entity invalid, reason:");
        console.log(entity.invalidateReason());

        return false;
    }

    return true;
    
}