import { ObjectId } from "mongodb";

export default class Entity{

    #entityId = null;

    constructor(id){
    
        try{

            

            if(!id){
                this.#entityId = new ObjectId();
                return;
            }

            this.#entityId = new ObjectId(id); // We dont need the object, we just need to test the id value provided
            

        }catch(err){
            
            
        
        }
    }

    getId(){

        return this.#entityId.toString();

    }

    getPureId(){

        return this.#entityId;
        
    }

}