export default class BaseEntity{

    #valid = true;

    #invalidateReason = "";

    #data = false;

    constructor(data){

        this.#data = data;

    }

    invalidate(reason){

        this.#invalidateReason = reason;

        this.#valid = false;

    }

    invalidateReason(){

        return this.#invalidateReason;

    }

    isValid(){
        

        return this.#valid;

    }

    getData(){

        return this.#data;

    }

}