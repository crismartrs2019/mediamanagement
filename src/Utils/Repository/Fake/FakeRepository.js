export default class FakeRepository{

    #calls = {};

    setCallTo(name,methodToCall){

        this.#calls[name] = methodToCall;

    }   

    callTo(name){

        const methodToCall = this.#calls[name];

        return this[methodToCall];

    }
}