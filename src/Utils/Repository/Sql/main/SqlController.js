export default class SqlController{
    
    constructor(){


    }

    create(data){

        return new Promise((resolve,reject)=>{


           this.repository.execute({ok:resolve,error:reject},this.queryCreate(data));

        });

    }

    read(data){

        return new Promise((resolve,reject)=>{

            this.repository.execute({ok:resolve,error:reject},this.queryRead(data));

        });

    }

    update(data){

        return new Promise((resolve,reject)=>{

            this.repository.execute({ok:resolve,error:reject},this.queryUpdate(data));

        });

    }

    delete(data){

        return new Promise((resolve,reject)=>{

            this.repository.execute({ok:resolve,error:reject},this.queryDelete(data));

        });

    }

}