import ProductsRepository from "./ProductsRepository.js";
import SqlController from "../main/SqlController.js";

export default class RepositoryInstance extends SqlController{

    repository = new ProductsRepository();
    
    constructor(){

        super();

    }

    queryCreate(data){

        return {
            query:"INSERT INTO PRODUCTS(NAME,ID) VALUES (?,?)",
            params:[data["name"],data["id"]]
        }

    }

    queryRead(){

        return {
            query:"SELECT * FROM PRODUCTS"
        }

    }

 
}