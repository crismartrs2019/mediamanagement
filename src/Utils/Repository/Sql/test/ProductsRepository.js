import SqlRepository from "../repository/SqlRepository.js";
import mysql2 from "mysql2";

export default class ProductsRepository extends SqlRepository{

    static repository = false;

    constructor(){

        super();

        ProductsRepository.repository = mysql2.createPool({
            host: 'localhost',
            user: 'root',
            database: 'test_js',
            waitForConnections: true,
            connectionLimit: 10,
            maxIdle: 10, // max idle connections, the default value is the same as `connectionLimit`
            idleTimeout: 60000, // idle connections timeout, in milliseconds, the default value 60000
            queueLimit: 0,
            enableKeepAlive: true,
            keepAliveInitialDelay: 0
        });

    }


    execute(response,data){

        this.executeProccess(response,data,ProductsRepository.repository);

    }


}