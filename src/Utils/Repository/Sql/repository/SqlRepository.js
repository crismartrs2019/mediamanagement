import mysql2 from "mysql2";
import SqlWriteResult from "../../../Result/Sql/SqlWriteResult.js";
import SqlReadResult from "../../../Result/Sql/SqlReadResult.js";

export default class SqlRepository{

    constructor(){

    }

    
    executeProccess(response,data,repository){

        if(repository==false){
            response["error"]({error:505,message:"repository not selected"});
            return;
        }   

        const params = data["params"]!=undefined?data["params"]:[];

        repository.query(data["query"],params,(err,rows,fields)=>{

            if(err){
                
                
                console.log(err);
                console.log("response");
                console.log(response);
                response["error"]({error:505,message:err});
                return;
            }

            if(rows.length!=undefined){

                response["ok"](new SqlReadResult(rows));

                return;
            }
            
            response["ok"](new SqlWriteResult({
                affectedRows: rows["affectedRows"],
                writed: params.length!=0? params[params.length-1]:""
            }));

        });


    }



}