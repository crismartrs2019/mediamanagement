export default class RepositoryTest{

    constructor(){

    }


    static operation(responseData){
        return new Promise((resolve,reject)=>{

            if(responseData["error"]){
                reject(responseData["data"]);
                return;
            }

            resolve(responseData["data"]);

        });

    }

}