import RepositoryTest from "./RepositoryTest.js";


export default class RepositoryOperationTest{

    #operation = 0;

    constructor(operation){

        this.#operation = operation;

    }

    do(dataResponse){

        return RepositoryTest.operation(dataResponse);

    }


}