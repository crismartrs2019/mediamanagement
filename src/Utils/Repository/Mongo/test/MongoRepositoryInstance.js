import MongoDbObject from "../repository/MongoDbObject.js";
import MongoRepository from "../Repository/MongoRepository.js";

export default class MongoRepositoryInstance extends MongoRepository{

    static repository  = new MongoDbObject({
        uri:"mongodb://127.0.0.1:27017/",
        options:{
            socketTimeoutMS:2000
        },
        database:"test_js"
    });

    constructor(){
        super();
    }

    getRepository(){

        return MongoRepositoryInstance.repository;

    }
}