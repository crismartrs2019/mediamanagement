import MongoController from "../main/MongoController.js";
import MongoRepositoryInstance from "./MongoRepositoryInstance.js";

export default class ControllerInstance extends MongoController{

    repository = new MongoRepositoryInstance();

    constructor(){
        super();
    }    

    queryCreate(data){
        return {
            query:[{name:"test works"}],
            collection:"test_collection"
        }
    }

    queryRead(data){

    }

    queryUpdate(data){

    }

    queryDelete(data){

    }


}