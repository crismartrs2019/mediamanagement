export default class MongoController{


    constructor(){

    }

    create(data){

        return new Promise((resolve,reject)=>{

            this.repository.create({ok:resolve,error:reject},this.queryCreate(data));

        });

    }

    read(data){

        return new Promise((resolve,reject)=>{

            this.repository.read({ok:resolve,error:reject},this.queryRead(data));

        });

    }   

    update(data){

        return new Promise((resolve,reject)=>{

            this.repository.update({ok:resolve,error:reject},this.queryUpdate(data));

        });

    }

    delete(data){

        return new Promise((resolve,reject)=>{

            this.repository.delete({ok:resolve,error:reject},this.queryDelete(data));

        });

    }

}