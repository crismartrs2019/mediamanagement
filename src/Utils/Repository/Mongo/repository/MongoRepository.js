export default class MongoRepository{

    constructor(){

    }

    create(response,data){
        
        this.getRepository().create(data,response);

    }

    read(response,data){

        this.getRepository().read(data,response);        

    }

    update(response,data){
        this.getRepository().update(data,response);

    }


    delete(response,data){

        this.getRepository().delete(data,response);

    }


}