import { MongoClient } from "mongodb";

export default class MongoDbObject{
    
    #dbRepository = false;

    #database = "";

    constructor(config){
        
        console.log(config);

        this.#dbRepository = new MongoClient(config["uri"],config["options"]);

        this.#database = config["database"];

    }

   

    create(data,response){
        
        
            const db = this.#dbRepository.db(this.#database);

            db.collection(data["collection"]).insertMany(data["query"]).then((result)=>{

                response["ok"](result);

            }).catch((err)=>{
                console.log(err);
                response["error"](":(");
            });
        
    }

    read(data,response){


            const db = this.#dbRepository.db(this.#database);

            db.collection(data["collection"]).find(data["query"],data["options"]).toArray().then((result)=>{

                response["ok"](result);

            }).catch((err)=>{
                console.log(err);
                response["error"]();
            });



    }

    update(data,response){

        
        const db = this.#dbRepository.db(this.#database);

        db.collection(data["collection"]).updateMany(data["query"],data["update"],data["options"]).then((result)=>{

            //console.log(result);
            response["ok"](result);

        }).catch((err)=>{
            console.log(err);
            response["error"]();
        });

8

    }

    delete(data,response){

    
        const db = this.#dbRepository.db(this.#database);

        db.collection(data["collection"]).deleteMany(data["query"]).then((result)=>{
            
            response["ok"](result);

        }).catch((err)=>{
            //console.log(err);
            response["error"](err);
        });


    }

}