import Fs from "node:fs/promises";


export default class LocalRepository{

    constructor(){

    }

    create(File,name){

        const path = this.queryCreate();  

        return Fs.writeFile(path+name,File);

    }

    read(name){

        const path = this.queryRead();

        return Fs.readFile(path+name); 

    }

}