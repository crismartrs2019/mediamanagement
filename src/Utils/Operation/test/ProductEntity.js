import Entity from "../../Entity/Entity.js";
import BasicFilter from "../../Filters/BasicFilter.js";


export default class ProductEntity extends Entity{

    #name = null;

    #price = null;
    
    constructor(data){
        super(data["id"]);

        this.#setName(data["name"]);
        this.#setPrice(data["price"]);
    }

    #setName(name){
        if(this.filterName(name)==false){
            this.invalidate();
            return;
        }

        this.#name = name;
    }

    getName(){

        return this.#name;

    }

    #setPrice(price){

        if(this.filterPrice(price)==false){
            this.invalidate();
            return;
        }

        this.#price = price;
    }

    getPrice(){

        return this.#price;

    }


}
