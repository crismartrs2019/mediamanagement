import ProductEntity from "./ProductEntity.js";
import basicFilter from "../../Filters/BasicFilter.js";


export default class ProductAdd extends ProductEntity{

    constructor(data){
        super(data);
    }

    filterName(name){

        return basicFilter(name).required().isType("string").valid();

    }

    filterPrice(price){

        return basicFilter(price).required().valid();

    }


}