import ProductEntity from "./ProductEntity.js";
import basicFilter from "../../Filters/BasicFilter.js";



function anyValue(value){

    if(value==null)return false;

    return true;
}


export default class ProductUpdate extends ProductEntity{

    constructor(data){
        super(data);
    }

    filterName(name){

        return basicFilter(name).required().isType("string").valid() || anyValue(this.getPrice());

    }

    filterPrice(price){

        return basicFilter(price).required().valid() || anyValue(this.getName());

    }


}