export default class Operation{

    #abort = false;

    #repository = false;

    #validations = [];

    constructor(){

    }

    do(){

        return new Promise(async (resolve,reject)=>{
            
            try{

                if(this.#validations.length>0){

                    await Promise.all(this.#validations);

                }

                if(this.#abort){

                    reject({error:505,message:"something went wrong on operation"});
    
                    return;
                }
    
                this.main({ok:resolve,error:reject});

            }catch(err){

                console.log(err);

                reject({error:505,message:err});

            }            

        });



    }

    abortOperation(){

        this.#abort = true;

    }


    setRepository(repository){

        this.#repository = repository;

    }

    setValidations(validations){

        this.#validations = validations;

    }

    repository(){

        return this.#repository;

    }

}