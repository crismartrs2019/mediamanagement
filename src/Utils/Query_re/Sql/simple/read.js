export default function simpleReadQuery(fields){

    var fieldResult = "";

    fields.forEach((field)=>{
        
        if(fieldResult!=""){
            fieldResult+=",";
        }

        fieldResult+=field;


    });

    return fieldResult;

}