export default function simpleUpdateQuery(fields){

    var fieldResult = "";
    
    var params = "";

    fields.forEach((field)=>{
        
        if(fieldResult!=""){
            fieldResult+=",";
            params+=",";
        }

        fieldResult+=field;

        params+="?";

    });


    return {
        fields:fieldResult,
        params:params
    };

}   