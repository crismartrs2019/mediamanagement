import ComparisionMethods from "../../../Utils/Comparision/utils/methods.js";

export default function getComparision(comparision){

    switch(comparision){

        case ComparisionMethods.EQUAL_THAN:

            return "=";

        case ComparisionMethods.DIFFERENT_THAN:

            return "!=";

        case ComparisionMethods.GREATHER_OR_EQUAL:

            return ">=";

        case ComparisionMethods.GREATHER_THAN:
           
            return ">";

        case ComparisionMethods.LESS_OR_EQUAL:
            
            return "<=";

        case ComparisionMethods.LESS_THAN:

            return "<"
        
        default:
            return "";
    }

}