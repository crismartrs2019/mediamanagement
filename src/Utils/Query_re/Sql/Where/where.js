export default class Where{

    #queryValue="";

    #params = [];

    constructor(field,logical,value){

        this.#setCondition(field,logical,value);

    }

    and(field,logical,value){

        this.#queryValue+=" AND ";
        this.#setCondition(field,logical,value);

        return this;

    }

    or(field,logical,value){

        this.#queryValue+=" OR ";
        this.#setCondition(field,logical,value);

        return this;

    }

    query(){

        return this.#queryValue;

    }

    getParams(){

      return this.#params;


    }

    #setCondition(field,logical,value){

        if(value==undefined)return;

        this.#queryValue+=field+logical+"?";

        this.#params.push(value);
    }

}
