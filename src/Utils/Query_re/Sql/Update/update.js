export default class Update{

    #update = "";
    #params = [];
  
    constructor(){
  
    }
  
    set(field,value){
  
      if(value==undefined)return;
  
      if(this.#update!="")this.#update+=",";
  
      this.#update += field+"=?";
  
  
      this.#params.push(value);
  
    }
  
    query(){
  
      return this.#update;
  
    }
  
    getParams(){
  
      return this.#params;
  
    }
  
  }
  