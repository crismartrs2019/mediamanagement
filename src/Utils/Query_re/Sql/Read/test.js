import conditionsGetter from "./conditions.js";
import ConditionChain from "../../Utils/ConditionChain/ConditionChain.js";
import lessOrEqual from "../../Utils/Comparision/lessOrEqual.js";

export default function testSqlQuery(){


    const conditions = new ConditionChain();

    conditions.and(lessOrEqual("quantity",2)).or(lessOrEqual("field",5));

    const result = conditionsGetter(conditions.getConditions());

    console.log(result);

}