import getComparision from "../utils/comparision/getComparision.js";
import getOperator from "../utils/operator/getOperator.js";

export default function conditionsGetter(conditions){

    var conditionQuery = "";

    conditions.forEach(conditionWrapper => {

        if(conditionQuery!=""){

            conditionQuery+=" "+getOperator(conditionWrapper.type)+" ";

        }

        conditionQuery+=conditionWrapper.condition.field;

        conditionQuery += getComparision(conditionWrapper.condition.comparision);

        conditionQuery+="?";

    });

    return conditionQuery;

}