import ConditionChain from "../Utils/ConditionChain/ConditionChain.js";
import Sentence from "./Sentence.js";

export default class Query extends Sentence{

    #conditions = new ConditionChain();


    constructor(fields){

        super(fields);

    }

    where(condition){
        
        return this.#conditions.and(condition);

    }   

    getConditions(){

        return this.#conditions;

    }

}