import Entity from "../Entity/Entity.js";


export default class EntityFiels extends Entity{


    #fields = {};

    constructor(data){

        super(data["id"]);

        if(!data["fields"])return;
        
        if(typeof data["fields"]!="object")return;

        this.#setFields(data["fields"]);

    }


    fieldRequired(name){

        return this.#fields[name];

    }


    #setFields(fields){

        Object.keys(fields).forEach((key)=>{

            this.#fields[key] = true;

        });

    }

}