import baseComparision from "./base.js";
import methods from "./utils/methods.js";


export default function lessThan(field,value){

    return {

        ...baseComparision(field,value),
        comparision:methods.LESS_THAN

    }

}


