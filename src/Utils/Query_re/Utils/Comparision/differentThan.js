import baseComparision from "./base.js";
import comparisionMethods from "./utils/methods.js";

export default function differentThan(field,value){

    return {
        ...baseComparision(field,value),
        comparision:comparisionMethods.DIFFERENT_THAN
    }

}