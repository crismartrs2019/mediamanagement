import baseComparision from "./base.js"
import comparisionMethods from "./utils/methods.js";

export default function equalThan(field,value){
    
    return {
        ...baseComparision(field,value),
        comparision:comparisionMethods.EQUAL_THAN
    };

}