import baseComparision from "./base.js";
import methods from "./utils/methods.js";


export default function greatherThan(field,value){

    return {

        ...baseComparision(field,value),
        comparision:methods.GREATHER_THAN

    }

}