export default class ConditionChain{


    #conditions = [];

    constructor(){
        
    }

    and(condition){

        this.#conditions.push({condition:condition,type:0});

        return this;

    }

    or(condition){

        this.#conditions.push({condition:condition});

        return this;

    }

    not(condition){

        this.#conditions.push({condition:condition,type:1});

        return this;

    }


    getConditions(){

        return this.#conditions;

    }

}