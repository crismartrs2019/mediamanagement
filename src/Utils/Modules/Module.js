export default class Module{

    #provider = false;

    constructor(){

    }

    setModulesProvider(provider){

        this.#provider = provider;

    }


    module(name){

        return this.#provider.module(name);

    }

}