export default class ModulesManager{

    #modules = {};

    constructor(){

    }


    add(name,moduleInstance){

        moduleInstance.setModulesProvider(this);

        this.#modules[name] = moduleInstance;

    }


    module(name){

        return this.#modules[name];

    }


}