export default class Data{

    #data = [];

    constructor(){

    }

    setField(name,value){

        this.#data[name] = value;

    }

    getField(fieldName){

        return this.#data[fieldName];

    }

    getData(){

        return this.#data;

    }


}