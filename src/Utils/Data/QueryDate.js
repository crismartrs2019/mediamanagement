import basicFilter from "../Filters/BasicFilter.js";
import BaseEntity from "../Entity/BaseEntity.js";

export default class QueryDate extends BaseEntity{

    #id = "";

    #month = 0;

    #day = 0;

    #year = 0;



    constructor(data){

        super();

        if(this.#eval(data)==false){
            this.invalidate();
            return;
        }

    }

    #eval(data){

        if(this.#setId(data["id"])==false){
            return false;
        }

        if(this.#setMonth(data["month"])==false){
            return false;
        }

        if(this.#setDay(data["day"])==false){
            return false;
        }


        if(this.#setYear(data["year"])==false){
            return false;
        }

        return true;
    }



    #setId(id){
        return true;
    }

    #setMonth(month){

        if(basicFilter(month).isType("number").valid()){
            this.#month = month;
            return true;
        }

        return false;
    }

    #setDay(day){

        if(basicFilter(day).isType("number").valid()){
            this.#day = day;
            return true;
        }

        return false;
    }

    #setYear(year){

        if(basicFilter(year).isType("number").valid()){
            this.#year = year;
            return true;
        }


        return false;
    }

}