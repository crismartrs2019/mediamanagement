import basicFilter from "../Filters/BasicFilter.js";

export default class Fields{

    #fields= {};

    constructor(fields){

        this.#fields = fields;

        this.#evalFields(fields);

    }

    required(name){

        return this.#fields[name];

    }       


    #evalFields(fields){

        Object.keys(fields).forEach((key)=>{

            if(fields[key]){

                this.#fields[key] = true;

            }

        });

    }

}