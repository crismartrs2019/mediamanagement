import basicFilter from "../Filters/BasicFilter.js";
import BaseEntity from "../Entity/BaseEntity.js";

export default class Page extends BaseEntity{

    #page = 1;

    constructor(pageParam){

        super();

        const page = parseInt(pageParam);

        if(!basicFilter(page).required().isType("number").isValid()){

            this.invalidate();

            return;
        }


        if(page>1){
            this.#page = (page-1)*10;
            return;
        }

        this.#page = 0; //we gonna specify the index of the data on the query for memory

    }

    page(){

        return this.#page;

    }

}