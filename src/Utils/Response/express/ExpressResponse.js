export default class ExpressResponse{

    #response = false;

    constructor(response){

        this.#response = response;

    }

    result(data){

        this.#response.json(data);

    }

    error(data,type){

        this.#response.status(505);
        
        this.#response.json(data);

    }


}