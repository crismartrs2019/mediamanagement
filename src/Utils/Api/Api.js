import superagent from "superagent";

export default class Api{

    #appId = "";

    #token = "";

    constructor(token,appId){

        this.#token = token;

        this.#appId = appId;

    }


    post(url,data){


        const result = superagent.post(url);

        result.set("Content-Type","application/json");

        result.set("Authorization","Bearer "+this.#token);

        result.send(data);

        return new Promise((resolve,reject)=>{

            result.end((err,res)=>{
                if(err){

                    reject(err);

                    return;
                }
                

                resolve(res);

            })
                    
        });
        

    }

}