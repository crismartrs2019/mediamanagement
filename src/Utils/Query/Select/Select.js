import Conditionable from "../Query/Conditionable/Conditionable.js";

export default class Select extends Conditionable{

    #queryFields = "";

    constructor(tableName){

        super(tableName);

    }


    fields(fields,Schema){

        fields.forEach((field)=>{  

            if(this.#queryFields!="")this.#queryFields+=",";

            this.#queryFields+=Schema[field];

        });

    }

    build(){

        const condition = this.getCondition();

        return {
            query:"SELECT "+this.#queryFields+" FROM "+this.getTable()+" "+condition.query(),
            params:condition.getParams()

        }

    }
    

}