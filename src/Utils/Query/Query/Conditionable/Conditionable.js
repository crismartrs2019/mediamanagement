import Where from "../../where/Where.js";
import Table from "../Table/Table.js";

export default class Conditionable extends Table{

    #condition; 

    constructor(tableName){

        super(tableName);

    }

    where(field,comparision,value){

        this.#condition = new Where(field,comparision,value);

        return this.#condition;

    }

    getCondition(){

        return this.#condition;

    }

}
// new Update("Products")
//update.where().and()
