export default class Table{

    #table = "";

    constructor(TableName){

        this.#table = TableName;

    }

    getTable(){

        return this.#table;

    }

}