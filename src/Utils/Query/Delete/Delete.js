import Conditionable from "../Query/Conditionable/Conditionable.js";

export default class Delete extends Conditionable{

    constructor(tableName){

        super(tableName);

    }

    build(){

        const condition = this.getCondition();

        return {

            query:"DELETE FROM "+this.getTable()+" "+condition.query(),
            params:condition.getParams()

        }

    }

}