import Conditionable from "../Query/Conditionable/Conditionable.js";

export default class Update extends Conditionable{

  #update = "";
  #params = [];

  #tableName = "";
 
  constructor(tableName){

    super();

    this.#tableName = tableName;

  }

  set(field,value){

    if(value==undefined || value==null)return;

    this.#setField(field,"?");

    this.#params.push(value);

  }

  setOrNull(field,value){

    if(value==undefined || value==null){

      this.#setField(field,"NULL");

      return;
    }

    this.#setField(field,"?");

    this.#params.push(value);

  }

  #setField(field,schema){

    if(this.#update!="")this.#update+=",";

    this.#update += field+"="+schema;

  }
  
  query(){

    return this.#update;

  }

  getParams(){

    return this.#params;

  }

  build(){

      const condition = this.getCondition();

      return {
        query:"UPDATE "+this.#tableName+" SET "+this.query()+" "+condition.query(),
        params:[...this.getParams(),...condition.getParams()]
      }
  }

}
