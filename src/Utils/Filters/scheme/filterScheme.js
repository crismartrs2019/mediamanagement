import { filterRequired, filterIsType, filterValue } from "../filters/filters.js";

function filterValueWrapper(filter,data,next){

    return (filterValue)=>{

        if(next==false)return createFiltersScheme(data,false);

        if(filter(filterValue,data)==false)return createFiltersScheme(data,false);

        return createFiltersScheme(data,true);
    
    }

}

function filterTypeWrapper(filter,data,next){

    return (type)=>{

        if(next==false)return createFiltersScheme(data,false);

        if(filter(data,type)==false)return createFiltersScheme(data,false);

        return createFiltersScheme(data,true);

    }

}


function filterRequiredWrapper(filter,data,next){

    return ()=>{

        console.log("required");

        if(next==false)return createFiltersScheme(data,false);

        if(filter(data)==false)return createFiltersScheme(data,false);

        return createFiltersScheme(data,true)
    }

}



export default function createFiltersScheme(data,next){

    
    return {
        required:filterRequiredWrapper(filterRequired,data,next),
        isType:filterTypeWrapper(filterIsType,data,next),
        value:filterValueWrapper(filterValue,data,next),
        isValid:()=>{
            return next;
        }
    }

}