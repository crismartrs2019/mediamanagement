export default class SqlReadResult{

    #found = false;
    #data = [];

    constructor(data){

        if(data.length>0)this.#found = true;

        this.#data = data;

    }


    result(){  

        return this.#data;

    }


    found(){

        return this.#found;

    }

}