export default class SqlWriteResult{

    #writed = false;

    #resultData = "";

    constructor(data){

        console.log(data);

        if(data["affectedRows"]>0)this.#writed = data["affectedRows"];

        this.#resultData = data["writed"];

    }

    result(){

        return this.#resultData;

    }

    writed(){

        return this.#writed;

    }

}