export default function UserIsType(UserData,types,UserModule){

    return new Promise(async (resolve,reject)=>{

        try{

            const user = await UserModule.read(UserData);

            console.log("user:");
            console.log(user);

            if(!user){

                reject({error:505,message:"User doesnt exists"});
                return;
            
            }

            for(let i=0;i<types.length;i++){

                if(user.type==types[i]){

                    resolve();

                    return;
                }

            }

            reject({error:505,message:"User doesnt have permission"});


        }catch(err){

            console.log(err);

            reject({error:505,message:"Validation went wrong"});

        }

    });


}