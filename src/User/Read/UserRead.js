import Operation from "../../Utils/Operation/main/Operation.js";

export default class UserRead extends Operation{


    #user = null;

    constructor(UserFilterRead){

        super();

        if(UserFilterRead.isValid()==false){

            this.abortOperation();

            return;
            
        }

        this.#user = UserFilterRead.getData();

    }

    async main(response){   

        try{

            const result = await this.repository().read({
                ...(this.#user.getPureId()?{_id:this.#user.getPureId()}:{}),
                ...(this.#user.getEmail?{email:this.#user.getEmail()}:{})
            });
            
            response.ok(result[0]);

        }catch(err){

            console.log(err);

            response.error({error:505,message:"Someting went wrong"});

        }

    }

}