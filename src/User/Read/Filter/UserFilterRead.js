import BaseEntity from "../../../Utils/Entity/BaseEntity.js";
import basicFilter from "../../../Utils/Filters/BasicFilter.js";

export default class UserFilterRead extends BaseEntity{

    constructor(UserData){

        super(UserData);

    }

    #eval(){

        const user = this.getData();

        if(basicFilter(user.getEmail()).isType("string").isValid()==false){

            this.invalidate();

            return; 
        }

    }

}