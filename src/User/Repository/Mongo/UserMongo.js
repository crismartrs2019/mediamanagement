import MediaMongoRepository from "../../../Repository/Media/mongo/MediaMongoRepository.js";
import MongoController from "../../../Utils/Repository/Mongo/main/MongoController.js";

export default class UserMongo extends MongoController{ 

    repository = new MediaMongoRepository();

    constructor(){

        super();

    }

    queryCreate(UserData){

        return {
            query:[{
                _id:UserData.getPureId(),
                name:UserData.getName(),
                type:UserData.getType(),
                email:UserData.getEmail()
            }],
            collection:"user"
        }

    }


    queryRead(UserData){

        return {
        
            query:UserData,
            collection:"user"

        }

    }


}