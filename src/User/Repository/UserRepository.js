import UserMongo from "./Mongo/UserMongo.js";

export default class UserRepository{

    static mongo = new UserMongo();

    static create(UserData){

        return UserRepository.mongo.create(UserData);

    }   

    static read(UserData){


        return UserRepository.mongo.read(UserData);

    }

}