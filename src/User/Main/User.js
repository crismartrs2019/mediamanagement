import Module from "../../Utils/Modules/Module.js";
import UserRepository from "../Repository/UserRepository.js";
import UserCreate from "../Create/UserCreate.js";
import UserData from "../Data/UserData.js";
import UserFilterCreate from "../Create/Filter/UserFilterCreate.js";
import UserFilterRead from "../Read/Filter/UserFilterRead.js";
import UserRead from "../Read/UserRead.js";


export default class User extends Module{

    constructor(){

        super();

    }

    create(UserData){

        const filter = new UserFilterCreate(UserData);

        const operation = new UserCreate(filter);

        operation.setRepository(UserRepository);

        return operation.do();

    }

    read(Entity){

        const filter = new UserFilterRead(Entity);

        const operation = new UserRead(filter);

        operation.setRepository(UserRepository);

        return operation.do();

    }

}