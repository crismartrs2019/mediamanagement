import Entity from "../../Utils/Entity/Entity.js";

export default class UserData extends Entity{   

    #data = {};

    constructor(data){

        super(data["id"]);

        this.#data = {
            name: data["name"],
            email: data["email"],
            type: data["type"]
        }

    }

    getName(){  

        return this.#data["name"];

    }

    getEmail(){

        return this.#data["email"];

    }

    getType(){

        return this.#data["type"];

    }

}