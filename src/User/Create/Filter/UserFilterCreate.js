import BaseEntity from "../../../Utils/Entity/BaseEntity.js";
import basicFilter from "../../../Utils/Filters/BasicFilter.js";

export default class UserFilterCreate extends BaseEntity{

    constructor(UserData){

        super(UserData);

        this.#eval();

    }

    #eval(){

        const user = this.getData();

        if(basicFilter(user.getName()).required().isType("string").isValid()==false){

            this.invalidate();
            
            return;
        }

        if(basicFilter(user.getEmail()).isType("string").required().isValid()==false){


            this.invalidate();

            return;

        }

        if(basicFilter(user.getPureId()).required()==false){

            this.invalidate();

            return;

        }

    }



}