import Operation from "../../Utils/Operation/main/Operation.js";

export default class UserCreate extends Operation{

    #user = null;

    constructor(UserFilterCreate){

        super();

        if(UserFilterCreate.isValid()==false){

            this.abortOperation();

            return;
        }   
        
        this.#user = UserFilterCreate.getData();

    }


    async main(response){

        try{


            const userExists = await this.repository().read({email:this.#user.getEmail()});

            if(userExists.length>0){

                response.error({error:505,message:"User already exists"});

                return;
            }

            const result = await this.repository().create(this.#user);

            response.ok(result);

        }catch(err){

            console.log(err);

            response.error({error:505,message:"something went wrong"});

        }

    }


}