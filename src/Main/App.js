import ModulesManager from "../Utils/Modules/ModulesManager.js";
import UserMain from "../User/Main/Main.js";
import MediaMain from "../Media/Main/Main.js";
import TopicMain from "../Topic/Main/Main.js";


export default function App(Express){

    const moduleManager = new ModulesManager();

    UserMain(moduleManager);

    MediaMain(moduleManager,Express);

    TopicMain(moduleManager,Express);

    return moduleManager;

}