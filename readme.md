# Api Modules
El sistema se encuentra construido de manera independiente al framework 
que permita la conexión con http, por lo cual este sistema se puede probar sin necesidad de 
una petición http, simplemente llamando a App(), extrayendo el módulo y ejecutando su operación
ej: App().module("user").create(UserData).

Para iniciar la App con Express configurado es necesario ejecutar el archivo "api.js" de la siguiente forma:  node --env-file=.env" api.js
Además es esencial configurar una ruta para los archivos en él archivo .env con FILES_BASE_PATH = Path

## App
    La app se encuentra almacenada en la función App() en la Carpeta "src/Main/App.js"
    esta función devuelve un contenedor de módulos y que pueden ser accedidos mediante el método "module(name:string)", aquí se encuentran definidos los modules user, media, topic.
    Internamente en La función "App()" se acopla express para hacer accesibles los módulos mediante http 
    Ej: route -> ModuleManager.module("media").create(MediaData) -> response
    
    Esto nos permite tener un sistema desacoplado al framework y que se puede probar sin express, de manera directa.
## Media
    Todas las rutas son accedidas mediante "/media"
### create
    
    POST: "/",
    DATA: {
        media:{name:String,type:String,file:Binary},
        user:{id:String}
    }
### read
        
    GET:"/{idMedia}",

### list

    GET: "/list"
    QUERY_PARAMS: {page:Integer}

## Topic
    Todas las rutas se encuentran bajo "/topic" en la Api
### create
    POST: "/"
    Data: {
        "topic":{name:String,types:Array}
        "user":{id:String}
    }
### read
    POST: "/list"
    DATA: {
        media:{name?:String}
    }
    QUERY_PARAMS: {page:Integer}
### addMedia
    
    POST: "/{idTopic}/media/{idMedia}/add"
    Data:{
        "user":{id:String}
    }

### getMedias
    
    GET : "/{idTopic}/medias/list"
    QUERY_PARAMS: {page:Integer}

### list 

    GET: "/list"
    QUERY_PARAMS: {page:Integer}