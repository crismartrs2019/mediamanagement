import App from "./src/Main/App.js";
import express from "express";
import bodyParser from "body-parser";

const expressApp  = express();

expressApp.use(bodyParser.json());

const app = App(expressApp);


expressApp.listen(8085,()=>{

    console.log("Listen on 8085");

});