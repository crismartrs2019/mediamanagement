import UserData from "./src/User/Data/UserData.js";
import App from "./src/Main/App.js";
import MediaData from "./src/Media/Data/MediaData.js";
import Entity from "./src/Utils/Entity/Entity.js";
import TopicData from "./src/Topic/Data/TopicData.js";
import Page from "./src/Utils/Data/Page.js";

import TopicRepository from "./src/Topic/Repository/TopicRepository.js";


const app = App();

//6621ea01e0152767ab865c05 reader user
// 6621d43c82d10dfe18246f2c  admin user

//topic: 6621f5c454c049db27e123c9

// media: 6621f1fb174ca11273d9322a

const topic = new Entity({id:"6621a482ebeaff7725294359"});

const page = new Page(1);

const media = new Entity({id:"6621f1fb174ca11273d9322a"})


app.module("topic").read(topic).then((res)=>{

    console.log(res);

}).catch((err)=>{

    console.log(err);

});


